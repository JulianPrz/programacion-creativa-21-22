#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:Imagen y píxeles
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+DATE: 09-04-22
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase;font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><h4>%d</h4><br><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Índice
  - [[Imagen en Processing][Imagen en Processing]]
  - [[Filtros de imagen][Filtros de imagen]]
  - [[Manipulando píxeles][Manipulando píxeles]]
  - [[Copiar y pegar píxeles][Copiar y pegar píxeles]]
  - [[Captura de imagen del sketch][Captura de imagen del sketch]]
* Resumen
  - Aprenderemos a cargar imágenes en Processing y aplicar una serie de efectos
  - Además veremos como manipular los píxeles de una imágen y capturar la imagen para exportar resultados
  - Enunciaremos el E3
* Imagen en Processing
  - Para almacenar datos de tipo imagen lo hacemos con =PImage=
  - Para cargar la imagen lo haremos con la función src_processing[:exports code]{loadImage("nombreDeLaImagen.jpg");}
  - Recuerda! Si estamos en estilo activo esta función tiene que estar dentro de =void setup(){}=
  #+reveal: split:t
  - Para incorporar una imagen podemos arrastrar y soltarla directamente en el /IDE/, o crear la carpeta "data" y pegarla ahí
  - Los formatos de imagen que soporta Processing son: =.gif=, =.jpg=, =.tga=, y =.png=
** Ejemplo para cargar una imagen desde /URL/
   #+begin_src processing
PImage img;

void setup() {
  size(400, 400);
  img = loadImage("https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg");
}

void draw() {
  image(img, 0, 0, width, height);
}
   #+end_src
** Ejercicio en clase: reloj con imágenes
   - Tenemos 10 imágenes con cada dígito ([[https://gitlab.com/JulianPrz/programacion-creativa-21-22/-/archive/main/programacion-creativa-21-22-main.zip?path=code/06/sketch_00_imagesClock/data][Descargar aquí]])
   - Vamos a cargarlas todas primeramente y las mostramos de izquierda a derecha
   - Después haremos que se cargen las imágenes en función de la hora que sea para hacer un reloj
   - Para ello tenemos las funciones =hour()=, =minute()= y =second()=
   #+begin_src processing :tangle yes ../../../../code/06/sketch_00_imagesClock/sketch_00_imagesClock.pde :mkdirp yes :exports none
PImage im[] = new PImage[10];
String imFile[] = {"0.jpg", "1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg", "9.jpg", };
int wIm = 125;
int hIm = 218;

void settings(){
  size(wIm*2, hIm*3);
}

void setup() {
  for (int i = 0; i < 10; i = i+1) {
    im[i] = loadImage(imFile[i]);
  }
}

void draw() {
  
  /////////////////HORAS/////////////////////////////
  
  int h = hour();
  int h_dec = int(h/10);
  int h_uni = h - h_dec *10;

  image(im[h_dec], 0, 0);
  image(im[h_uni], wIm, 0);

  ////////////////MINUTOS////////////////////////////

  int m = minute();
  int m_dec = int(m/10);
  int m_uni = m - m_dec *10;

  image(im[m_dec], 0, hIm);
  image(im[m_uni], wIm, hIm);

  ////////////////SEGUNDOS///////////////////////////

  int s = second();
  int s_dec = int(s/10);
  int s_uni = s - s_dec *10;

  image(im[s_dec], 0, hIm*2);
  image(im[s_uni], wIm, hIm*2);
}
#+end_src
* Filtros de imagen
  - En Processing tenemos una serie de filtros, al estilo photoshop, que se aplican con =filter()=
  - Se aplica a las imágenes que se hayan pintado anteriormente.
  - Además podemos "tintar" las imágenes de color con =tint()= y revertir el tintado con =noTint()=
  - Filtros: =THRESHOLD=, =GRAY=, =OPAQUE=, =INVERT=, =POSTERIZE=, =BLUR=, =ERODE=, =DILATE=
** Probando filtros
#+begin_src processing :tangle yes ../../../../code/06/sketch_01_imagesFilters/sketch_01_imagesFilters.pde :mkdirp yes
        PImage img;
        int i = 0;
        
        void setup() {
          size(800, 400);
          img = loadImage("https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg");
        }
        
        void draw() {
          image(img, 0, 0, 400, height); //Imagen a la que se aplicará filter()
          switch(i) {
          case 0:
            noTint();
            filter(BLUR, 6);
            break;
          case 1:
            filter(POSTERIZE, 4);
            break;
          case 2:
            filter(INVERT);
            break;
          case 3:
            filter(THRESHOLD);
            break;
          case 4:
            filter(GRAY);
            break;
          case 5:
            tint(0, 153, 204);
            break;
          }
        
          image(img, 400, 0, 400, height); //Imagen original
        }
        
        void mousePressed(){
          i++;
          if(i>5){
            i=0;
          }
        }
   #+end_src
* Manipulando píxeles
  - Con la función =get()= podemos obtener el color de la coordenadas de una imagen que le indiquemos
  - Con =loadPixels()= crea un array =pixels[]= con la info de color de cada pixel de la imagen
  - Podemos modificar la info de cada pixel, si hacemos esto es importante terminar con =updatePixel()=
  #+reveal: split:t
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: Cómo se disponen info de píxel en array
https://processing.org/static/5de945a82ce86f15ba9cda9179a38823/8ba2d/pixelarray.jpg
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 180px :display block
#+CAPTION: Cómo se indica índice de pixel en el array
https://processing.org/static/c499c57411c56e9dbe25f38c7aaffb56/8ba2d/pixelarray2d.jpg
#+REVEAL_HTML: </div>
** Ejercicio clase
- Modifica el código para que la mitad de los píxeles de la imagen sean negros
#+begin_src processing
size(200, 200);

loadPixels();

for (int i = 0; i < pixels.length; i++ ) {
  float rand = random(255);
  color c = color(rand);
  pixels[i] = c; 
}

updatePixels();
#+end_src
** Ejemplo
#+begin_src processing :tangle yes ../../../../code/06/sketch_03_imageArray/sketch_03_imageArray.pde :mkdirp yes
  PImage img;
int gridSize = 5; // variable del tamaño del celda
float maxDist = 100;

void settings() {
  img= loadImage("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/ff5604ba-57a5-48f8-a91f-85c1de625100/d4xc2j2-50178a00-e68d-45ea-8430-9ca107860e7f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2ZmNTYwNGJhLTU3YTUtNDhmOC1hOTFmLTg1YzFkZTYyNTEwMFwvZDR4YzJqMi01MDE3OGEwMC1lNjhkLTQ1ZWEtODQzMC05Y2ExMDc4NjBlN2YuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.4L6I24PbyYROnGFCPTovTDUR1_1ecMJQ82fT-VV63QU", "jpg"); // Cargamos la imagen
  size(img.width, img.height); // Tamaño del canvas adaptado al tamaño de la imagen
}

void setup() {
  img.loadPixels(); //carga la info de los pixeles al array pixels[]
  println(img.width);
  println(img.height);
  println(img.pixels.length);
  noStroke();
}

void draw() {
  background(0);
  gridSize = int(map (mouseX, 0, width, 1, 20)); //mapeamos valores mouseX con el ancho de la celda de retícula
  
  for (int i=0; i<img.width; i++) {
    for (int j=0; j<img.height; j++) {
      color c = img.get(i*gridSize, j*gridSize); //extraemos el color del pixel que quede debajo de la celda de la rejilla
      int x = i*gridSize; //y escalamos la rejilla en proporción del ancho de cada celda
      int y = j*gridSize;
      
      float d = dist(x, y, mouseX, mouseY); //Efecto de linterna
      float alpha = map(d, 0, maxDist, 255, 0);
      if (d < maxDist) {
        fill(c, alpha);
      } else {
        fill(0);
      }
      
      rect(x, y, gridSize, gridSize);
    }
  }
}
#+end_src
* Copiar y pegar píxeles
- La función =copy()= nos permite copiar una región de píxeles de una imagen y pegarla en otras dimensión igual, escalada o desproporcionada
** Ejemplo
#+begin_src processing :tangle yes ../../../../code/06/sketch_04_imageCopy/sketch_04_imageCopy.pde :mkdirp yes
PImage img;

void setup() {
  size(400, 400);
  img = loadImage("https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg");
  img.resize(width, height);
  background(0);
}

void draw() {
  //image(img, 0, 0, 400, height);
  int x = int(random(width));
  int y = int(random(height));
  copy(img, x, y, int(random(50)), int(random(50)), x, y, int(random(50)), int(random(50))); 
}
#+end_src
* Captura de imagen del sketch
- Para capturar la imagen de un sketch lo hacemos con =saveFrame()=
- Normalmente se inserta dentro de una condición tras pulsar una tecla o click
** Ejemplo
#+begin_src processing
  PImage img;
  
  void setup() {
    size(400, 400);
    img = loadImage("https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg");
    img.resize(width, height);
    background(0);
  }
  
  void draw() {
    //image(img, 0, 0, 400, height);
    int x = int(random(width));
    int y = int(random(height));
    copy(img, x, y, int(random(50)), int(random(50)), x, y, int(random(50)), int(random(50))); 
  }
  
  
  void keyPressed() {
    if (key=='s' || key=='S') {
      saveFrame("export-######.png");
    }
#+end_src
* Siguiente ->                                                     :noexport:
   :PROPERTIES:
   :reveal_background: #FFCC00
   :END:
#+REVEAL_HTML: <a href="https://julianprz.gitlab.io/programacion-creativa-21-22/main/docs/01_Processing/03-formas-simples-personalizadas.html" class="r-fit-text" target="_blank">3-Formas simples / personalizadas</h2>
* Contenidos                                                       :noexport:
* Template                                                         :noexport:
** Índice
# Generar TOC
# org-reveal-manual-toc
** Indice 2 columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
#+REVEAL_HTML: </div>
** 2 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/02/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/02/]]
#+REVEAL_HTML: </div>
** 3 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

** 1 imagen
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/02/]]

** export processing code                                         :noexport:
#+begin_src processing :tangle no ../../code/04/sketch_00_example/sketch_00_example.pde :mkdirp yes

#+end_src
# org-babel-tangle
# Tangle the current file. Bound to C-c C-v t.
# Tangle the current code block. C-u C-c C-v C-t
# With prefix argument only tangle the current code block.

** inline processing code                                         :noexport:
# src_processing[:exports code]{;}

** Symbols
- \downarrow
  
