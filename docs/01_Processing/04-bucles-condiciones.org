#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:Bucles y Condiciones
#+SUBTITLE: Programación Creativa
#+AUTHOR: Julián Pérez
#+DATE: 23-02-22
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase;font-size:2em" >%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><h4>%d</h4><br><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Índice
  - [[Resumen][Resumen]]
  - [[Movimiento \downarrow][Movimiento]]
  - [[Más ejemplos de condicionales \downarrow][Más ejemplos de condicionales]]
  - [[*Loop =while()=][Loop =while()=]]
  - [[Loop =for()=][Loop =for()=]]
  - [[=PGraphics()=][=PGraphics()=]]
  - [[Aleatoriedad con =noise()=][Aleatoriedad con =noise()=]]
* Resumen
- Resolveremos dudas acerca del E1
- A través de bucles y condicionales aprenderemos a animar cosas en nuestro canvas
- Enunciaremos E2 para entregar el *11 de marzo*
* Movimiento \downarrow
** Mover es sumar/restar y asignar
- Vamos a intentar mover una bola en el ejeX y que se mantenga en la mitad del ejeY
- Observamos que ocurre
#+begin_src processing
  int x = 0;
  void setup(){
    size(300,300);
  }
  
  void draw(){
    background(0);
    ellipse(x,height/2,50,50);
    x++; //Esto es lo mismo que x=x+1;
  }
#+end_src
** Ponemos una condición
- Con =if()= vamos a decirle que cuando llegue al final vuelva a empezar
- Con =>= comparamos si /A/ es *mayor que* /B/
- Efecto de [[https://youtu.be/JHlUEJXaxfY?t=152][juegos arcade]]
#+begin_src processing
int x = 0;
void setup() {
  size(300, 300);
}

void draw() {
  background(0);
  ellipse(x, height/2, 50, 50);
  x++;
  if(x > width){ //Si llega al extremo derecho del canvas vuelve a empezar de 0 la bola
    x = 0;
  }
}

#+end_src
** ¿Y si queremos ir a una velocidad diferente?
- Creamos una variable para la *posición* y otra para la *velocidad*
#+begin_src processing
    int xPos = 0;
    int xVel = 2;
    void setup() {
      size(300, 300);
    }
    
    void draw() {
      background(0);
      ellipse(xPos, height/2, 50, 50);
      xPos = xPos + xVel;
      if(xPos >= width){ //Si llega al extremo derecho del canvas vuelve a empezar de 0 la bola
        xPos = 0;
      }
    }
    
#+end_src
** Vamos a hacer que rebote
- Al rebotar cambiamos la *dirección*, creamos una variable para indicar la dirección
- Si va hacia la derecha será *positiva* y si va hacia la izquierda *negativa*
- Observamos qué ocurre
#+begin_src processing
    int xPos = 0;
    int xVel = 2;
    int xDir = 1;
    void setup() {
      size(300, 300);
    }
    
    void draw() {
      background(0);
      ellipse(xPos, height/2, 50, 50);
      xPos = xPos + xVel * xDir;
      if(xPos >= width){ //Rebota la pelota al llegar a la pared derecha
        xDir = -1;
      }
    }
    
#+end_src
** Mejoramos el rebote
- Haremos que rebote en cuanto toque el extremo derecha de la bola con la pared, no el centro
- Definimos variable para el radio de la bola
- Hacemos que bote en la pared izquierda también
#+begin_src processing
    int xPos = 0;
    int xVel = 2;
    int xDir = 1;
    int r = 25;
    void setup() {
      size(300, 300);
      xPos = width/2; //hacemos que empiece desde la mitad porque sino no avanza
    }
    
    void draw() {
      background(0);
      ellipse(xPos, height/2, r*2, r*2);
      xPos = xPos + xVel * xDir;
      if(xPos < 0+r || xPos >= width-r){ //Rebota en ambas paredes
        xDir = xDir * -1; // multiplicamos y asignamos, para cambiar a su valor contrario en función de para qué lado vaya
      }
    }
    
  #+end_src
** Ejercicio en clase
- Vamos a hacer lo mismo para el ejeY (yPos, yVel, yDir)
** Ejemplo de código modularizado
#+begin_src processing :tangle ../../code/04/sketch_00_movimiento/sketch_00_movimiento.pde :mkdirp yes
float xPos; //Trabajamos mejor con floats
float yPos;
float xVel = 2.8;
float yVel = 2.2;
int xDir = 1;
int yDir = 1;
int r = 25;

void setup() {
  size(300, 300);
  xPos=width/2;
  yPos=height/2;
}

void draw() {
  background(0);
  muestraBola();
  mueveBola();
  tocaBorde();
}

void muestraBola() {
  ellipse(xPos, yPos, r*2, r*2);
}

void mueveBola() {
  xPos = xPos + (xVel * xDir);
  yPos = yPos + (yVel * yDir);
}

void tocaBorde() {
  if (xPos > width-r || xPos < 0+r) {
    xDir *= -1;
  }

  if (yPos < 0+r || yPos > height-r) {
    yDir *= -1;
  }
}
#+end_src
* Más ejemplos de condicionales \downarrow
** =if else= Si SÍ -> esto, si NO -> esto otro
- Activamos un contador con =frameCount=
- Si es menor a 200 haz una cosa, si no, haz otra
#+begin_src processing
  void setup(){
    size(400,400);
  }

  void draw(){  
    if(frameCount<200){
      fill(234,21,34);
    }else{
      fill(34,123,12);
    }
    rect(mouseX,mouseY,50,50);
  }
#+end_src
** Se pueden encadenar varios ifs con =else if=
#+begin_src processing
  void setup(){
    size(400,400);
  }
  
  void draw(){  
    if(frameCount<200){
      fill(234,21,34);
  
    }else if(frameCount<400){
      fill(32,213,56);
    }else{
      fill(0);
    }
    rect(mouseX,mouseY,50,50);
  }
#+end_src
** Controlar estados o /flags/
#+begin_src processing :tangle ../../code/04/sketch_01_ifelse/sketch_01_ifelse.pde :mkdirp yes
boolean state = false;
int colorState = 0;

void setup(){
}

void draw(){
  if (state){           //si es true, ejecuta esto
    colorState = 0;
  }else{                //y si no, si es false, ejecuta esto otro
    colorState = 255;
  }
  background(colorState);
}
void mousePressed(){
 state = !state;        //cambiamos el state a su valor opuesto cada vez que clickamos
}
#+end_src
* Loop =while()=
** Ejemplo básico
- =while()= se ejecuta mientras se cumpla lo que aparezca entre paréntesis
#+begin_src processing
int i = 0;

while (i < 80) {
  line(30, i, 80, i);
  i = i + 5;
}    
#+end_src
** Evitar bucle infinito
- Dependiendo de nuestro programa podemos evitar que while se quede en un bucle infinito con =break=
#+begin_src processing
int i = 0;

void setup(){
  size(400,400);
}

void draw(){
  ellipse(mouseX,mouseY,20,20);

  while (i < 99999999) {
    if(i == 300){
    line(30, i, 80, i);
    break;
    }
    i = i + 5;
  } 
}
#+end_src
* Loop =for()=
- El bucle o loop =for= nos sirve para controlar una sequencia de repeticiones
- Se estructura de la siguiente manera
#+begin_src processing
    for(init;test;update){
      funciones
      }
#+end_src
- Tiene el siguiente orden:
  1. Se declara /init/
  2. El /test/ es evaluado si es =false= o =true=
  3. Si /test/ es =true=, salta al paso 4. Si es =false=, salta al 6
  4. Ejecuta las funciones dentro del bloque
  5. Ejecuta el /update/ y salta al paso 2
  6. Sal del loop
** Probemos con un ejemplo
#+begin_src processing
    for(int i = 0; i < width; i=i+10){
      line(i,0,i,height);
      }
#+end_src
- Primera iteración:
  1. /init/ -> i = 0;
  2. /test/ -> 0 < width ?
  3. =true= -> paso 4, =false= -> se acaba el loop
  4. Ejecuta -> line(0,0,0,height);
  5. /update/ -> i=10, pasa a la iteración 2
- Segunda iteración:
  1. ahora i vale 10
  2. /test/ -> 10 < width ?
  3. =true= -> paso 4, =false= -> se acaba el loop
  4. Ejecuta -> line(10,0,10,height);
  5. /update/ -> i=20, pasa a la iteración 3...
** Condicional dentro de loop for
#+begin_src processing
  size(400,200);
  background(0);
  for(int i = 0; i < width; i=i+20){
    stroke(255);
    line(i,0,i,height);
    if(i>width/2){
      stroke(183, 161,50);
      line(i,0,i,height);
    }
  }
#+end_src
** =for()= anidado
- Imaginaros que queremos hacer una matrix de círculos como en la siguiente imagen
#+attr_html: :height 200px :display block
#+caption: Matriz de círculos. fuente: happycoding.io
https://happycoding.io/tutorials/processing/images/for-loops-5.png
#+reveal: split:t
- Podríamos hacerlo de la siguiente manera
 #+begin_src processing
   size(300, 300);
   
   for (int circleX = 75; circleX <= 225; circleX += 75) {
     ellipse(circleX, 75, 50, 50);
   }
   
   for (int circleX = 75; circleX <= 225; circleX += 75) {
     ellipse(circleX, 150, 50, 50);
   }
   
   for (int circleX = 75; circleX <= 225; circleX += 75) {
     ellipse(circleX, 225, 50, 50);
   }
 #+end_src
- ... pero es bastante redundante
#+reveal: split:t
- En estos casos podemos anidar un =for()= dentro de otro =for()=
#+begin_src processing :tangle ../../code/04/sketch_02_for/sketch_02_for.pde :mkdirp yes
size(300, 300);

for (int circleY = 75; circleY <= 225; circleY += 75) {
  for (int circleX = 75; circleX <= 225; circleX += 75) {
    ellipse(circleX, circleY, 50, 50);
  }
}
#+end_src
- Hacemos lo mismo pero con menos líneas de código
** Ejercicio en clase 1
- Crea una rejilla con líneas utilizando un for anidado
** Ejercicio en clase 2
- Vamos a crear el efecto de ruido de una tele antigua
- Crea una rejilla de cuadrados con ancho definido por una variable que complete todo el canvas
- Utiliza un for anidado para ello
- Dale un gris random de relleno a cada uno
- Mapea el ancho del cuadrado con mouseX y varia la resolución de esa rejilla
* =PGraphics()=
- Hay que entender =PGraphics()= como si hiciesemos una captura de imagen de la forma que estamos dibujando
- Y después podemos utilizar esa imagen como y tantas veces como queramos
- Podemos volver a editar esa captura y volver a usarla
** Ejemplo básico
#+begin_src processing
PGraphics pg;

void setup() {
  size(100, 100);
  pg = createGraphics(40, 40);
}

void draw() {
  pg.beginDraw();
  pg.background(100);
  pg.stroke(255);
  pg.line(20, 20, mouseX, mouseY);
  pg.endDraw();
  image(pg, 9, 30); 
  image(pg, 51, 30);
}
#+end_src
** Capas superpuestas
- Imagina que quieres que un dibujo no se esté superponiendo constantemente
- Sino que se queda por debajo de una primera capa
#+begin_src processing :tangle ../../code/04/sketch_03_pgraphics/sketch_03_pgraphics.pde :mkdirp yes
PGraphics pg1, pg2;
int currentLayer = 1;

void setup() {
  size(600, 600);
  pg1=createGraphics(width, height);
  pg2=createGraphics(width, height);
}

void draw() {
  background(0);

  if (currentLayer == 1) {
    pg1.beginDraw();
    pg1.fill(160, 200, 0);
    pg1.noStroke();
    if (mousePressed) {
      pg1.ellipse(mouseX, mouseY, 40, 40);
    }
    pg1.endDraw();
  } else if (currentLayer==2) {
    pg2.beginDraw();
    pg2.fill(150, 20,120);
    pg2.noStroke();
    if (mousePressed) {
      pg2.ellipse(mouseX, mouseY, 40, 40);
    }
    pg2.endDraw();
  }

  image(pg1, 0, 0);
  image(pg2, 0, 0);
  
}
void keyPressed() {
  if (key=='1') {
    currentLayer = 1;
  } else if (key=='2') {
    currentLayer = 2;
  }
}
#+end_src

* Aleatoriedad con =noise()=
- Está basado en el algoritmo de Ken Perlin de 1980
- Se utilizó en la película de TRON en 1982
- Para generar gráficos procedurales: texturas, formas, terrenos, etc.
- A diferencia de =random()=, con =noise()= obtenemos una secuencia más harmónica y natural
- Siempre nos devolverá entre 0 y 1
** Ejemplo noise 1D
#+begin_src processing :tangle ../../code/04/sketch_04_noise1D/sketch_04_noise1D.pde :mkdirp yes
  float x;
  float tx=1;
  void setup() {
    size(300,300);
  }
  
  void draw() {
    background(0);
    x = noise(tx);
    x = map(x, 0, 1, 0, width);
  
    stroke(255);
    ellipse(x,height/2,20,20);
    tx+=0.005;
  }
#+end_src
** Ejemplo noise() 1D vs random()
#+begin_src processing :tangle ../../code/04/sketch_05_noiseVsRandom/sketch_05_noiseVsRandom.pde :mkdirp yes
float xoff = 0.0;
void setup(){
  size(400,400);
  background(0);
  
}

void draw(){
  background(0);
  noFill();
  stroke(255);
  beginShape();
  for(int i=0; i<width;i++){
    vertex(i*10,random(1)*200);
  }
  endShape();
    beginShape();
  for(int i=0; i<width;i++){
    vertex(i*10,200+noise(xoff)*200);
    xoff = xoff + 0.01;
  }
  endShape();
}
#+end_src
** Ejemplo noise 2D
#+begin_src processing :tangle ../../code/04/sketch_06_noise2D/sketch_06_noise2D.pde :mkdirp yes
  float tx1=1;
  float ty1=2;
  
  void setup() {
    size(300,300);
  }
  
  void draw() {
    background(0);
    float x1 = noise(tx1);
    float y1 = noise(ty1);
  
    x1 = map(x1, 0, 1, 0, width);
    y1 = map(y1, 0, 1, 0, height);
  
    stroke(255);
    ellipse(x1,y1,20,20);
    tx1+=0.005;
    ty1+=0.005;
  }
#+end_src
** Más recursos sobre =noise()=
- https://www.youtube.com/watch?v=J2lFNXZuXPc
- https://www.youtube.com/watch?v=Qf4dIN99e2w&list=PLRqwX-V7Uu6bgPNQAdxQZpJuJCjeOr7VD

* Siguiente ->
   :PROPERTIES:
   :reveal_background: #FFCC00
   :END:
#+REVEAL_HTML: <a href="https://julianprz.gitlab.io/programacion-creativa-21-22/main/docs/01_Processing/05-texto-tipografia.html" class="r-fit-text" target="_blank">5-Texto y Tipografía</h2>
* Contenidos                                                       :noexport:
* Template                                                         :noexport:
** Índice
# Generar TOC
# org-reveal-manual-toc
** Indice 2 columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
#+REVEAL_HTML: </div>
** 2 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 45%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/02/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 45%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/02/]]
#+REVEAL_HTML: </div>
** 3 Columnas
#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :heigh 200px :display block
#+caption: 
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:left; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

#+REVEAL_HTML: <div class="column" style="float:right; width: 33%">
#+attr_html: :height 290px :display block
#+CAPTION:
[[../../img/]]
#+REVEAL_HTML: </div>

** 1 imagen
#+attr_html: :height 400px :display block
#+caption: 
[[../../img/02/]]

** export processing code                                         :noexport:
#+begin_src processing :tangle no ../../code/04/sketch_00_example/sketch_00_example.pde :mkdirp yes

#+end_src
# org-babel-tangle
# Tangle the current file. Bound to C-c C-v t.
# Tangle the current code block. C-u C-c C-v C-t
# With prefix argument only tangle the current code block.

** Symbols
- \downarrow
