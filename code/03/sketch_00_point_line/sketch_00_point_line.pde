/*
Puntos y lineas

Cambia el valor de la variable 'd' para escalar la forma
Las cuatro variables configuran las posiciones basadas en el valor de 'd'
Ejercicio: une los puntos para mostrar el cubo 3D
*/

int d = 70;
int p1 = d;
int p2 = p1+d;
int p3 = p2+d;
int p4 = p3+d;

size(640, 360);
noSmooth();
background(0);
translate(140, 0); //Mueve el origen (0,0) a (140,0)

// Dibuja el cuadrado rojo
stroke(0,255,0);
line(p3, p3, p2, p3);
line(p2, p3, p2, p2);
line(p2, p2, p3, p2);
line(p3, p2, p3, p3);

// Dibuja puntos blancos
stroke(255);
point(p1, p1);
point(p1, p3); 
point(p2, p4);
point(p3, p1); 
point(p4, p2);
point(p4, p4);

// Dibuja líneas para mostrar el cubo 3D
// line();
// line();
// line();
// line();
// line();
// line();
// line();
// line();
// line();
// line();
// line();
// line();
