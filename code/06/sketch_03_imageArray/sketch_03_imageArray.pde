PImage img;
int gridSize = 5; // variable del tamaño del celda
float maxDist = 100;

void settings() {
  img= loadImage("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/ff5604ba-57a5-48f8-a91f-85c1de625100/d4xc2j2-50178a00-e68d-45ea-8430-9ca107860e7f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2ZmNTYwNGJhLTU3YTUtNDhmOC1hOTFmLTg1YzFkZTYyNTEwMFwvZDR4YzJqMi01MDE3OGEwMC1lNjhkLTQ1ZWEtODQzMC05Y2ExMDc4NjBlN2YuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.4L6I24PbyYROnGFCPTovTDUR1_1ecMJQ82fT-VV63QU", "jpg"); // Cargamos la imagen
  size(img.width, img.height); // Tamaño del canvas adaptado al tamaño de la imagen
}

void setup() {
  img.loadPixels(); //carga la info de los pixeles al array pixels[]
  println(img.width);
  println(img.height);
  println(img.pixels.length);
  noStroke();
}

void draw() {
  background(0);
  gridSize = int(map (mouseX, 0, width, 1, 20)); //mapeamos valores mouseX con el ancho de la celda de retícula
  
  for (int i=0; i<img.width; i++) {
    for (int j=0; j<img.height; j++) {
      color c = img.get(i*gridSize, j*gridSize); //extraemos el color del pixel que quede debajo de la celda de la rejilla
      int x = i*gridSize; //y escalamos la rejilla en proporción del ancho de cada celda
      int y = j*gridSize;
      
      float d = dist(x, y, mouseX, mouseY); //Efecto de linterna
      float alpha = map(d, 0, maxDist, 255, 0);
      if (d < maxDist) {
        fill(c, alpha);
      } else {
        fill(0);
      }
      
      rect(x, y, gridSize, gridSize);
    }
  }
}
