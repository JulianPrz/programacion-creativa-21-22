PImage img;
int i = 0;

void setup() {
  size(800, 400);
  img = loadImage("https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg");
}

void draw() {
  image(img, 0, 0, 400, height); //Imagen a la que se aplicará filter()
  switch(i) {
  case 0:
    noTint();
    filter(BLUR, 6);
    break;
  case 1:
    filter(POSTERIZE, 4);
    break;
  case 2:
    filter(INVERT);
    break;
  case 3:
    filter(THRESHOLD);
    break;
  case 4:
    filter(GRAY);
    break;
  case 5:
    tint(0, 153, 204);
    break;
  }

  image(img, 400, 0, 400, height); //Imagen original
}

void mousePressed(){
  i++;
  if(i>5){
    i=0;
  }
}
