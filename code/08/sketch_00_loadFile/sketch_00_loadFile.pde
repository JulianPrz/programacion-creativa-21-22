import ddf.minim.*;

Minim minim;
AudioPlayer player;

void setup(){
  minim = new Minim(this);
  player = minim.loadFile("I Got You.mp3");
}

void draw(){
  player.play();
}
