import processing.video.*;
Capture video; 

void setup() {
  size(640, 480);
  printArray(Capture.list());
  //video = new Capture(this, 720, 480, "Nombre de tu dispositivo de captura de video", 30); //Inicializamos el objeto video con los sig parámetros (this, String del nombre de dispositivo de captura que nos aparezca en Capture.list(), ancho, alto, framesPerSecond)
  video = new Capture(this, Capture.list()[0]); //Esta es otra forma de llamar a las diferentes webcams
  video.start(); //Iniciamos el video
}

//Metodo 1: este es mejor que el método 2. Ya que procesa el video fuera del draw
void captureEvent(Capture video){ //Evento que se ejecuta cada vez que hay un frame nuevo
  video.read();                    //Lee el frame actual
}
void draw() {
  //Método 2
  //if (video.available()) { //Paso previo para comprobar si la cámara está operativa
  //  video.read(); //Para obtener los pixeles en el momento concreto, un refresh de la imagen
  //}
  background(0);
  image(video, 0, 0);
}
